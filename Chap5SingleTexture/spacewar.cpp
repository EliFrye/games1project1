// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Draw planet with transparency
// Chapter 5 spacewar.cpp v1.0
// This class is the core of the game

#include "spaceWar.h"
#include "d3dx9math.h"
#include <string>
#include <cmath>

//=============================================================================
// Constructor
//=============================================================================
Spacewar::Spacewar()
{
	srand(time(NULL));
}

//=============================================================================
// Destructor
//=============================================================================
Spacewar::~Spacewar()
{
    releaseAll();           // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void Spacewar::initialize(HWND hwnd)
{
    Game::initialize(hwnd); // throws GameError
	
	if (!myTexture.initialize(graphics, MY_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "My texture initialization failed"));
	if (!myImage.initialize(graphics, 0,0,0, &myTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init my image"));
	myImage.setX(GAME_WIDTH/2 - (myImage.getWidth()*MY_IMAGE_SCALE)/2);
	myImage.setY(GAME_HEIGHT/2 - (myImage.getHeight()*MY_IMAGE_SCALE)/2);
	myImage.setScale(MY_IMAGE_SCALE);
	
	//Stuff for physics
	myPos.xPos = myImage.getX();
	myPos.yPos = myImage.getY();
	
	myVel.xVel = 60;
	myVel.yVel = 60;
	
	//meter:
	if (!meterTexture.initialize(graphics, METER_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Meter texture initialization failed"));
	if (!meter.initialize(graphics, 0,0,0, &meterTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init meter image"));
	meter.setX(GAME_WIDTH*.95);
	meter.setY(GAME_HEIGHT*.75);
	meter.setScale(METER_IMAGE_SCALE);
	
	//Stuff for physics
	meterPos.xPos = meter.getX();
	meterPos.yPos = meter.getY();
	
	if (!footballTexture.initialize(graphics, FOOTBALL_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "My texture initialization failed"));
	if (!football.initialize(graphics, 0,0,0, &footballTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init my image"));
	football.setX(GAME_WIDTH/2 - (football.getWidth()*FOOTBALL_IMAGE_SCALE)/2);
	football.setY(GAME_HEIGHT/2 - (football.getHeight()*FOOTBALL_IMAGE_SCALE)/2);
	football.setScale(FOOTBALL_IMAGE_SCALE);
	
	//Stuff for physics
	footballPos.xPos = football.getX();
	footballPos.yPos = football.getY();
	
	footballVel.xVel = 60;
	footballVel.yVel = 60;

	if (!belichickTexture.initialize(graphics, BELICHICK_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "My texture initialization failed"));
	if (!belichick.initialize(graphics, 0,0,0, &belichickTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init my image"));
	belichick.setX(GAME_WIDTH/2 - (belichick.getWidth()*BELICHICK_IMAGE_SCALE)/2);
	belichick.setY(GAME_HEIGHT - (belichick.getHeight()*BELICHICK_IMAGE_SCALE));
	belichick.setScale(BELICHICK_IMAGE_SCALE);
	belichickPos.xPos = myImage.getX();
	belichickPos.yPos = myImage.getY();

	belichickVel.xVel = 120;
	belichickVel.yVel = 120;

	belichickAirtime=0;

    return;
}

//=============================================================================
// Update all game items
//=============================================================================
void Spacewar::update()
{
	
	float dirX=0;
	float dirY=0;
	float footballDirX;
	float footballDirY;

	if(input->isKeyDown(VK_LEFT)&&!input->isKeyDown(VK_RIGHT)) {dirX = -1;}
	if(input->isKeyDown(VK_RIGHT)&&!input->isKeyDown(VK_LEFT)) {dirX = 1;}
	if(belichickPos.yPos<(GAME_HEIGHT - (belichick.getHeight()*BELICHICK_IMAGE_SCALE))&&( !input->isKeyDown(VK_UP) || belichickAirtime>2.0)){dirY = (belichickPos.yPos/GAME_HEIGHT);}//Fall
	if(input->isKeyDown(VK_UP) && belichickAirtime<2.0) {dirY = -(belichickPos.yPos/GAME_HEIGHT);}
	if(dirX+dirY!=0)//normalize
	{
		dirX/= sqrt(dirX*dirX+dirY*dirY);
		dirY/= sqrt(dirX*dirX+dirY*dirY);
	}
	belichickPos.yPos = belichick.getY() + (dirY * belichickVel.yVel) * frameTime;
	belichickPos.xPos = belichick.getX() + (dirX * belichickVel.xVel) * frameTime;
	if(belichickPos.yPos>=GAME_HEIGHT-(belichick.getHeight()*BELICHICK_IMAGE_SCALE))
		belichickAirtime=0.0;
	else
		belichickAirtime+=frameTime;
		
	belichick.setX(belichickPos.xPos);
	belichick.setY(belichickPos.yPos);


	footballDirX=1;
	footballDirY=0;
	footballPos.xPos = football.getX() + (footballDirX * footballVel.xVel) * frameTime;
	football.setX(footballPos.xPos);

//WRAP
//Belichick:
if(belichickPos.xPos*BELICHICK_IMAGE_SCALE<0)
{
	belichickPos.xPos=GAME_WIDTH - belichick.getWidth()*BELICHICK_IMAGE_SCALE;
	belichick.setX(belichickPos.xPos);
}

else if(belichickPos.xPos>GAME_WIDTH - belichick.getWidth()*BELICHICK_IMAGE_SCALE)
{
	belichickPos.xPos=0;
	belichick.setX(belichickPos.xPos);
}
//ball:
if(footballPos.xPos>GAME_WIDTH - football.getWidth()*FOOTBALL_IMAGE_SCALE)
{
	footballPos.xPos=0;
	footballPos.yPos=(GAME_HEIGHT - (rand() % GAME_HEIGHT)/2)-football.getHeight()*FOOTBALL_IMAGE_SCALE;
	football.setX(footballPos.xPos);
	football.setY(footballPos.yPos);
}
else if(footballPos.xPos*FOOTBALL_IMAGE_SCALE<0)
{
	footballPos.xPos=GAME_WIDTH - football.getWidth()*FOOTBALL_IMAGE_SCALE;
	footballPos.yPos=rand() % GAME_HEIGHT-football.getHeight()*FOOTBALL_IMAGE_SCALE;
	football.setX(footballPos.xPos);
	football.setY(footballPos.yPos);
}
	//if(footballPos.xPos+
//REFLECT
 

 ////////////////
// INPUT MODS
 ////////////////

 int directionX = 0;
 int directionY = 0;

}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void Spacewar::ai()
{}

//=============================================================================
// Handle collisions
//=============================================================================
void Spacewar::collisions()
{}

//=============================================================================
// Render game items
//=============================================================================
void Spacewar::render()
{
    graphics->spriteBegin();                // begin drawing sprites
	myImage.draw();
	meter.draw();
	football.draw();
	belichick.draw();

    graphics->spriteEnd();                  // end drawing sprites
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void Spacewar::releaseAll()
{


    Game::releaseAll();
    return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void Spacewar::resetAll()
{
  

    Game::resetAll();
    return;
}
