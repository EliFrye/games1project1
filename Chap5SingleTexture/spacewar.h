// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 5 spacewar.h v1.0

#ifndef _SPACEWAR_H             // Prevent multiple definitions if this 
#define _SPACEWAR_H             // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "graphics.h"
#include <time.h>
#include <random>

//=============================================================================
// This class is the core of the game
//=============================================================================
class Spacewar : public Game
{
private:

	// My defines
	#define MY_IMAGE_SCALE .48
	#define METER_IMAGE_SCALE .22
	#define BELICHICK_IMAGE_SCALE .1
	#define FOOTBALL_IMAGE_SCALE .3

// My vars
	/*std::default_random_engine random;
	std::uniform_real_distribution<float> distr;*/
	float footballXVel;
	float footballYVel;
	TextureManager footballTexture;
	Image football;
	TextureManager meterTexture;
	Image meter;
	float backgroundXVel;
	float backgroundYVel;
	TextureManager myTexture;
	Image myImage;
	float BelichickXVel;
	float BelichickYVel;
	TextureManager belichickTexture;
	Image belichick;
	struct position{
		float xPos;
		float yPos;
		position() {xPos = 0; yPos = 0;}
	} myPos, belichickPos, footballPos, meterPos;
	struct velocity{
		float xVel;
		float yVel;
		velocity() { xVel = 0; yVel = 0;}
	} myVel, belichickVel,footballVel;
	float belichickAirtime;
	
public:
    // Constructor
    Spacewar();

    // Destructor
    virtual ~Spacewar();

    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "
    void collisions();  // "
    void render();      // "
    void releaseAll();
    void resetAll();
};

//Stuff for physics


#endif
